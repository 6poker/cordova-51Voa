var util = (function(){
	var regUrl = /\/|http:\/\//i;
	var uniqueIdSeed = 0;
	function uniqueId(){
		return 'VOA-51-'+(++uniqueIdSeed);
	}

	function isNetworkOk(){
		var networkState = navigator.connection.type;
		if(networkState==Connection.WIFI || networkState==Connection.CELL_3G || networkState==Connection.CELL_4G)
			return true;
		else
			return false;
	}

	function getAbsoluteUrl(url, relativePath){
		if(url.charAt(0)=="/")
			return 'http://www.51voa.com'+url;
		else if(url.indexOf('http://')==0)
			return url;
		else{
			return relativePath+'/'+url;
		}
	}
	var sizeStandard = [1024*1024*1024, 1024*1024, 1024];
	var sizeUnit = ['GB','MB','KB'];
	function formatSize(size){
		for(var i=0;i<3;i++){
			if(size > sizeStandard[i]){
				return (size/sizeStandard[i]).toFixed(1)+sizeUnit[i];
			}
		}
		return size+'B';
	}
	return{
		uniqueId: uniqueId,
		isNetworkOk: isNetworkOk,
		getAbsoluteUrl: getAbsoluteUrl,
		formatSize: formatSize
	}
})();