var rootDirEntry;
function init(){
    VoaStorage.init();
    Play.init();
    DL.init();
    

    $('.btn-setting').tap(function(){
    	$('.doc').addClass('setting-zone');
    });
    $('.page-setting .btn-return').tap(function(){
    	$('.doc').removeClass('setting-zone');
    });
    $('.setting-item-sel').tap(function(){
    	$('.doc').addClass('cat-zone');
    });
    $('.page-cat .btn-return').tap(function(){
    	$('.doc').removeClass('cat-zone');
    });

    var tplSelFun = $("#catSelTpl").html().tmpl();
    $('.page-cat .ui-content').html(tplSelFun(Conf));
    $('.page-cat .ui-content .cat-sel-item').tap(function(){
    	var btn = $(this).find('.cat-sel-item-check-btn').toggleClass('cat-sel-item-unchecked');
    });

    $('.minibar .list-btn').tap(function(){
        $('.doc').addClass('play-zone');
    });

    $('.tab-my').tap(function(){
    	$('.ui-content').removeClass('dl-zone');
    	$('.tab-my').addClass('cur');
    	$('.tab-dl').removeClass('cur');
    });
    $('.tab-dl').tap(function(){
    	$('.ui-content').addClass('dl-zone');
    	$('.tab-dl').addClass('cur');
    	$('.tab-my').removeClass('cur');
    });

    window.resolveLocalFileSystemURL(
        cordova.file.externalRootDirectory, 
        function(dirEntry) {
            dirEntry.getDirectory(
                '51voa', 
                {create: true}, 
                function(_dirEntry) {
                    rootDirEntry = _dirEntry;
                },
                function(e){
                    console.log('文件路径有问题', e);
                }
            );
        },
        function(e){
            console.log('文件路径有问题', e);
        }
    );
}
document.addEventListener('deviceready', init, false);
