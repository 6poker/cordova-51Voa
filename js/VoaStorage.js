var VoaStorage = (function(){
	var userData;
	function init(){
		try{
			var localData = localStorage.VoaStorage;
			if(localData){
				userData = JSON.parse(localData);
			}
		}catch(e){
			console.error(e.message);
		}

		if(!userData){
			userData = {
				config: {
					list: ['technology','agriculture'],
					cacheCount: 2,  //每个分类下，检测是否有新音乐时，检测的文章个数
    				onlyWifi: true
				},
				song: {},
				uniqueId: 0
			}

			for(var i=0,len=Conf.list.length;i<len;i++){
				userData.song[Conf.list[i]] = [];
			}
		}
	}
	function getUserConfig(){
		return userData.config;
	}
	function addSong(song){
		if(!song.cat || Conf.list.indexOf(song.cat)<0) return;
		song.uid = ++userData.uniqueId;
		userData.song[song.cat].unshift(song);
		localStorage.VoaStorage = JSON.stringify(userData);
	}
	function removeSong(song){
		for(var i=0,songList=userData.song[song.cat],len=songList.length;i<len;i++){
			if(song.uid == songList[i].uid){
				songList.splice(i, 1);
				localStorage.VoaStorage = JSON.stringify(userData);
				break;
			}
		}
	}
	function getSongNameByCat(cat){
		var list = [],
			songList = userData.song[cat];
		songList.forEach(function(item){
			list.push(item.name);
		});
		return list;
	}
	function getSongByUid(uid, cat){
		var catList;
		if(cat) catList = [cat];
		else catList = userData.config.list;
		for(var i=0,len=catList.length;i<len;i++){
			for(var j=0,jlen=userData.song[catList[i]].length;j<jlen;j++){
				if(uid == userData.song[catList[i]][j].uid)
					return userData.song[catList[i]][j];
			}
		}
		return false;
	}
	function getStorage(){
		return userData;
	}
	return{
		init: init,
		getStorage: getStorage,
		addSong: addSong,
		removeSong: removeSong,
		getSongNameByCat: getSongNameByCat,
		getUserConfig: getUserConfig,
		getSongByUid: getSongByUid
	}
})();